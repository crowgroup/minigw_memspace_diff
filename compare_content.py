import json
import subprocess
import webbrowser
import os
import requests
import shutil
import hashlib


class ContentCompare(object):
    def __init__(self):
        """
        Constructor
        """
        self.prereq = ContentCompare.load_prerequisites()
        self.origin_base_file = self.prereq['src_base_file']
        self.origin_recent_file = self.prereq['src_recent_file']

        self.inputs_dir = os.path.dirname(self.origin_base_file)
        if os.path.isdir(self.inputs_dir):
            shutil.rmtree(self.inputs_dir)
        os.mkdir(self.inputs_dir)

        print 'url_base:', self.prereq['base_file_url']
        resp = requests.get(self.prereq['base_file_url'], verify=False,
                            auth=(self.prereq["auth"][0], self.prereq["auth"][1]))  # stream=True,
        with open(self.origin_base_file, 'w') as fp_base:
            # shutil.copyfileobj(resp.raw, fp_base)
            fp_base.write(resp.content)

        print 'url_recent:', self.prereq['recent_file_url']
        resp = requests.get(self.prereq['recent_file_url'], verify=False,
                            auth=(self.prereq["auth"][0], self.prereq["auth"][1]))  # stream=True,
        with open(self.origin_recent_file, 'w') as fp_recent:
            # shutil.copyfileobj(resp.raw, fp_recent)
            fp_recent.write(resp.content)

        self.dest_base_file = self.prereq['dest_base_file']
        self.dest_recent_file = self.prereq['dest_recent_file']
        self.outputs_dir = os.path.dirname(self.dest_base_file)
        if os.path.exists(self.outputs_dir):
            shutil.rmtree(self.outputs_dir)
        os.mkdir(self.outputs_dir)

    @staticmethod
    def load_prerequisites():
        """
        Load settings
        :return: json
        """
        with open('prerequisites.json') as fp:
            return json.load(fp)

    @staticmethod
    def is_files_identical(filename1, filename2):
        """
        Check the both files hashes are equal 
        :param filename1: str
        :param filename2: str
        :return: bool
        """
        hash1 = hash2 = ''
        try:
            with open(filename1, 'rb') as fb1:
                buf = fb1.read()
                hasher = hashlib.md5()
                hasher.update(buf)
                hash1 = hasher.hexdigest()
        except IOError, e:
            print filename1 + ' does not exist'
            # return False

        try:
            with open(filename2, 'rb') as fb2:
                buf = fb2.read()
                hasher = hashlib.md5()
                hasher.update(buf)
                hash2 = hasher.hexdigest()
        except IOError, e:
            print filename2 + ' does not exist'
            # Trick: first run last destination backup file does not exist, so we copy it from outputs.
            # Following re-run is need.
            shutil.copy(filename1, filename2)
            return True

        return len(hash1) > 0 and len(hash2) > 0 and hash1 == hash2

    def _extract_relevant_content_from_file(self, origin_file, dest_file):
        """
        Extract relevant fragments from original file and put them to destination file
        :param origin_file: str
        :param dest_file: str
        """
        with open(origin_file) as fp_orig:
            orig_content = fp_orig.readlines()

        with open(dest_file, 'w') as fp_dest:
            cur_fragment = 0
            inside_fragment = False
            for line in orig_content:
                if self.prereq['fragments'][cur_fragment]['start'] in line:
                    inside_fragment = True
                elif self.prereq['fragments'][cur_fragment]['end'] in line:
                    cur_fragment += 1
                    if cur_fragment >= len(self.prereq['fragments']):
                        return
                    inside_fragment = True if self.prereq['fragments'][cur_fragment]['start'] in line else False

                if inside_fragment:
                    fp_dest.write(line)

    def extract_relevant_content(self):
        """
        Extract relevant content from the certain base and recent files accordingly
        """
        self._extract_relevant_content_from_file(self.origin_base_file, self.dest_base_file)
        self._extract_relevant_content_from_file(self.origin_recent_file, self.dest_recent_file)

    def is_relevant_content_was_changed(self):
        """
        Check if the recent relevant content was changed since last build
        :return: bool
        """
        dest_recent_bkp_file = self.prereq['dest_recent_file_bkp']
        changed_resent = not self.is_files_identical(self.dest_recent_file, dest_recent_bkp_file)
        changed_prereq = not self.is_files_identical('prerequisites.json', 'prerequisites_bkp.json')
        changed = changed_resent or changed_prereq

        # Save the result is identical (True/False) to text file for outside usage (e.g. by Jenkins)
        workspace_dir = os.getenv('WORKSPACE', os.getcwd())
        print 'WORKSPACE:', workspace_dir
        file_name = os.getenv('TRANK_CHANGED', 'trank_changed.txt')
        print 'exchange file:', file_name
        file_path = os.path.join(workspace_dir, file_name)
        with open(file_path, 'w') as f:
            f.write(str(changed))

        return changed_resent, changed_prereq

    def compare_relevant_content(self):

        """
        Get diff recent - base and resent - last build accordingly.
        Save the diff to two HTMLs 
        :return: 
        """
        # Check if recent (SVN Trank) relevant content was changed
        changed_resent, changed_prereq = self.is_relevant_content_was_changed()
        if not changed_resent and not changed_prereq:
            print 'Nothing changed - nothing compare.'

            # Restore the both diff.html from backup
            if os.path.exists(self.prereq['base_diff_html_bkp']):
                shutil.copy(self.prereq['base_diff_html_bkp'], self.prereq['base_diff_html_file'])
            if os.path.exists(self.prereq['last_diff_html_bkp']):
                shutil.copy(self.prereq['last_diff_html_bkp'], self.prereq['last_diff_html_file'])
            return

        # Call the 'diff' utility and convert the results to HTML files
        with open(self.outputs_dir + '/base_diff.txt', 'w') as file_plain:
            subprocess.call(['diff', '-u', self.dest_base_file, self.dest_recent_file], stdout=file_plain)
        subprocess.call(["diff2html", "--input", "file", self.outputs_dir + "/base_diff.txt", "--style", "side",
                         "--file", self.prereq['base_diff_html_file']])

        with open(self.outputs_dir + '/last_diff.txt', 'w') as file_plain:
            if changed_prereq:
                file_plain.write('Paths are changed - compare does not relevant')
            else:
                subprocess.call(['diff', '-u', self.prereq['dest_recent_file_bkp'], self.dest_recent_file], stdout=file_plain)

        # last_diff.txt may be empty - check it
        with open(self.outputs_dir + '/last_diff.txt', 'r') as last_txt:
            lines_last = last_txt.readlines()
        if len(lines_last) == 0:
            with open(self.outputs_dir + '/last_diff.txt', 'w') as last_txt:
                last_txt.write('No changes')
        subprocess.call(["diff2html", "--input", "file", self.outputs_dir + "/last_diff.txt", "--style", "side",
                         "--file", self.prereq['last_diff_html_file']])

        # Backup dest recent and prerequisites for the next run
        shutil.copy(self.dest_recent_file, self.prereq['dest_recent_file_bkp'])
        shutil.copy('prerequisites.json', 'prerequisites_bkp.json')

        path_base_html = os.path.abspath(self.prereq['base_diff_html_file'])
        print path_base_html
        path_last_html = os.path.abspath(self.prereq['last_diff_html_file'])
        print path_last_html

        # Backup the both diff.html for the next usage
        shutil.copy(path_base_html, self.prereq['base_diff_html_bkp'])
        shutil.copy(path_last_html, self.prereq['last_diff_html_bkp'])

        if not os.getenv('WORKSPACE'):
            webbrowser.get('/usr/bin/google-chrome').open(path_base_html)
            webbrowser.get('/usr/bin/google-chrome').open(path_last_html)


def main():
    cmpr = ContentCompare()
    cmpr.extract_relevant_content()
    cmpr.compare_relevant_content()

if __name__ == "__main__":
    main()


