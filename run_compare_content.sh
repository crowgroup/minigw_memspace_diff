#!/bin/bash
PYENV_HOME=$WORKSPACE/.pyenv
echo $PYENV_HOME
if [ $WORKSPACE ]
then
    # Delete previously built virtualenv
    if [ -d $PYENV_HOME ]
    then
        rm -rf $PYENV_HOME
    fi

    # Create virtualenv and install necessary packages
    virtualenv --no-site-packages $PYENV_HOME
    source $PYENV_HOME/bin/activate
    pip install -r requirements.txt
fi

#curl -ku michaelf:worker9 "https://redmind/svn/Serenity/!svn/ver/7724/projects/MiniGW/Infrastructure/tags/77/Gen_Minigw/memspace.h" > inputs/recent.txt
#curl -ku michaelf:worker9 "https://redmind/svn/Serenity/!svn/ver/6479/projects/MiniGW/Infrastructure/tags/71/Gen_MiniGW/memspace.h" > inputs/base.txt

python compare_content.py
